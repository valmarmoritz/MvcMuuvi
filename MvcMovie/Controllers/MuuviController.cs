using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class MuuviController : Controller
    {
        private readonly MvcMuuviContext _context;

        public MuuviController(MvcMuuviContext context)
        {
            _context = context;
        }

		// GET: Muuvi
		// Requires using Microsoft.AspNetCore.Mvc.Rendering;
		public async Task<IActionResult> Index(string MuuviZhanr, string searchString)
		{
			// Use LINQ to get list of genres.
			IQueryable<string> genreQuery = from m in _context.Muuvi
											orderby m.Genre
											select m.Genre;

			var movies = from m in _context.Muuvi
						 select m;

			if (!String.IsNullOrEmpty(searchString))
			{
				movies = movies.Where(s => s.Title.Contains(searchString));
			}

			if (!String.IsNullOrEmpty(MuuviZhanr))
			{
				movies = movies.Where(x => x.Genre == MuuviZhanr);
			}

			var movieGenreVM = new MovieGenreViewModel();
			movieGenreVM.zhanrid = new SelectList(await genreQuery.Distinct().ToListAsync());
			movieGenreVM.muuvid = await movies.ToListAsync();

			return View(movieGenreVM);
		}


        // GET: Muuvi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var muuvi = await _context.Muuvi
                .SingleOrDefaultAsync(m => m.ID == id);
            if (muuvi == null)
            {
                return NotFound();
            }

            return View(muuvi);
        }

        // GET: Muuvi/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Muuvi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,ReleaseDate,Genre,Price,Rating")] Muuvi muuvi)
        {
            if (ModelState.IsValid)
            {
                _context.Add(muuvi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(muuvi);
        }

        // GET: Muuvi/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var muuvi = await _context.Muuvi.SingleOrDefaultAsync(m => m.ID == id);
            if (muuvi == null)
            {
                return NotFound();
            }
            return View(muuvi);
        }

        // POST: Muuvi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,ReleaseDate,Genre,Price,Rating")] Muuvi muuvi)
        {
            if (id != muuvi.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(muuvi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MuuviExists(muuvi.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(muuvi);
        }

        // GET: Muuvi/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var muuvi = await _context.Muuvi
                .SingleOrDefaultAsync(m => m.ID == id);
            if (muuvi == null)
            {
                return NotFound();
            }

            return View(muuvi);
        }

        // POST: Muuvi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var muuvi = await _context.Muuvi.SingleOrDefaultAsync(m => m.ID == id);
            _context.Muuvi.Remove(muuvi);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MuuviExists(int id)
        {
            return _context.Muuvi.Any(e => e.ID == id);
        }
    }
}
