﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace MvcMovie.Models
{
	public static class SeedData
	{
		public static void Initialize(IServiceProvider serviceProvider)
		{
			using (var context = new MvcMuuviContext(
				serviceProvider.GetRequiredService<DbContextOptions<MvcMuuviContext>>()))
			{
				// Look for any movies.
				if (context.Muuvi.Any())
				{
					return;   // DB has been seeded
				}

				context.Muuvi.AddRange(
					 new Muuvi
					 {
						 Title = "When Harry Met Sally",
						 ReleaseDate = DateTime.Parse("1989-1-11"),
						 Genre = "Romantic Comedy",
						 Rating = "R",
						 Price = 7.99M
					 },

					 new Muuvi
					 {
						 Title = "Ghostbusters ",
						 ReleaseDate = DateTime.Parse("1984-3-13"),
						 Genre = "Comedy",
						 Rating = "PG",
						 Price = 8.99M
					 },

					 new Muuvi
					 {
						 Title = "Ghostbusters 2",
						 ReleaseDate = DateTime.Parse("1986-2-23"),
						 Genre = "Comedy",
						 Rating = "PG",
						 Price = 9.99M
					 },

				   new Muuvi
				   {
					   Title = "Rio Bravo",
					   ReleaseDate = DateTime.Parse("1959-4-15"),
					   Genre = "Western",
					   Rating = "G",
					   Price = 3.99M
				   }
				);
				context.SaveChanges();
			}
		}
	}
}