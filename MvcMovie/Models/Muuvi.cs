﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcMovie.Models
{
    public class Muuvi
    {
		public int ID { get; set; }

		[Display(Name = "Pealkiri")]
		[StringLength(60, MinimumLength = 3)]
		[Required]
		public string Title { get; set; }

		[Display(Name = "Väljalaske kuupäev")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
		public DateTime ReleaseDate { get; set; }

		[Display(Name = "Žanr")]
		[RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
		[Required]
		[StringLength(30)]
		public string Genre { get; set; }

		[Display(Name = "Hind")]
		[Range(1, 100)]
		[DataType(DataType.Currency)]
		public decimal Price { get; set; }
		
        [Display(Name = "Reiting")]
		[RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
		[StringLength(5)]
		[Required]
		public string Rating { get; set; }

        public Muuvi()
        {
        }
    }
}
