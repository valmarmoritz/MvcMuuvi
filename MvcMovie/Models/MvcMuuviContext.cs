﻿using System;
using Microsoft.EntityFrameworkCore;

namespace MvcMovie.Models
{
    public class MvcMuuviContext : DbContext
    {
		public MvcMuuviContext(DbContextOptions<MvcMuuviContext> options)
            : base(options)
        {
		}

		public DbSet<MvcMovie.Models.Muuvi> Muuvi { get; set; }

        public MvcMuuviContext()
        {
        }
    }
}
