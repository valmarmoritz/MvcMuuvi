﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MvcMovie.Models
{
	public class MovieGenreViewModel
	{
		public List<Muuvi> muuvid;
		public SelectList zhanrid;
		public string MuuviZhanr { get; set; }
	}
}